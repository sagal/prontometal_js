import axiod from "https://deno.land/x/axiod@0.23.1/mod.ts";
import jsBigDecimal from "https://esm.sh/js-big-decimal";

const PRICE_LIST_NUMBER = 2

const clientId = Deno.env.get('CLIENT_ID')
const clientIntegrationId = Deno.env.get('CLIENT_INTEGRATION_ID')
const clientEcommerce = JSON.parse(Deno.env.get('CLIENT_ECOMMERCE'))

const endpoint = Deno.env.get('ENDPOINT')
const username = Deno.env.get('USERNAME')
const password = Deno.env.get('PASSWORD')
const opusPath = Deno.env.get('OPUS_PATH')
const enterprise = Deno.env.get('ENTERPRISE')

async function login() {
    const body = new URLSearchParams();
    body.set('opusPath', opusPath)
    body.set('enterprise', enterprise)
    body.set('username', username)
    body.set('password', password)


    let response = await axiod.post(
        `${endpoint}/OpusEcommerceAPI/ecommerce/user/login`,
        body,
        {
            headers: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            timeout: 500000,
        });
    return response.data
}

async function getArticles(loginToken) {
    let response = await axiod.get(
        `${endpoint}/OpusEcommerceAPI/ecommerce/products/getArticulos`,
        {
            headers: {
                'Authorization': "Bearer " + loginToken,
            },
            timeout: 500000,
        });
    return response.data
}

function getPriceFromLists(lists, taxPercentage) {
    if (lists && lists.length) {
        let priceList = lists.find((x) => x.lista == "2")
        if (priceList) {
            let tax = taxPercentage + 100
            let price = jsBigDecimal.round((priceList.precio * tax) / 100, 2, jsBigDecimal, "HALF_EVEN")
            return {
                "value": Number(price),
                "currency": priceList.moneda == "0" ? '$' : 'U$S',
            }
        }

    }
    return {
        "value": 0,
        "currency": '$',
    }
}

try {
    let loginToken = await login()
    let rawArticles = await getArticles(loginToken)
    for (let erpArticle of rawArticles) {
        let product = {
            sku: erpArticle.codigo,
            client_id: clientId,
            options: {
                merge: false,
            },
            integration_id: clientIntegrationId,
            ecommerce: Object.values(clientEcommerce).map(ecommerce_id => {
                return {
                    ecommerce_id: ecommerce_id,
                    variants: [],
                    properties: [
                        { "name": erpArticle.descripcion },
                        { "stock": erpArticle.stock > 0 ? erpArticle.stock : 0 },
                        {
                            "price": getPriceFromLists(erpArticle.listas, erpArticle.tasaIVA)
                        },
                        {
                            "metadata": {
                                "iva": `${erpArticle.tasaIVA}`
                            }
                        }
                    ]
                }
            })
        }
        await sagalDispatch(product)
    }
} catch (e) {
    console.log(e)
}